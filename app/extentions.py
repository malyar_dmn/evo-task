from flask import session
from flask_pymongo import PyMongo

mongo = PyMongo()

def check_user_in_collection(username):
    users_collection = mongo.db.users
    user = users_collection.find_one({'name': username})
    if (user is None):
        session['user'] = f'Привіт, {username}'
        return False
    else:
        session['user'] = f'Вже бачилися, {username}'
        return True
