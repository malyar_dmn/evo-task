from flask import Blueprint, redirect, render_template, request, session, url_for
from app.extentions import check_user_in_collection, mongo

main = Blueprint('main', __name__)

@main.route('/')
def index():
    user = ''
    if 'user' in session:
        user = session['user']

    return render_template('index.html', user=user)

@main.route('/add_user', methods=['POST'])
def add_user():
    users_collection = mongo.db.users
    user = request.form.get('add-user')
    session['user'] = user
    isUser = check_user_in_collection(user)

    if (isUser is not True):
        users_collection.insert_one({'name': user})
    
    return redirect(url_for('main.index'))

@main.route('/users', methods=['GET'])
def get_users():
    users_collection = mongo.db.users
    users = users_collection.find()

    return render_template('users.html', users=users)
