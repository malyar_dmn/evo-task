from flask import Flask

from .main.routes import main
from .extentions import mongo

def create_app():
    app = Flask(__name__)
    app.secret_key = 'secret'
    app.config['MONGO_URI'] = 'mongodb+srv://dmn:95241243@cluster0.h01fl.mongodb.net/mydb?retryWrites=true&w=majority'
    mongo.init_app(app)

    app.register_blueprint(main)

    return app